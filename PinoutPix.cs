﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace mdr32cube
{
    public partial class PinoutPix : Panel
    {
        [Category("Custom"), Description("Cache of system icons"), DefaultValue(null)]
        public Pinout pinout { get; set; }

        public string SelectedPeriph;

        public PinoutPix()
        {
            InitializeComponent();

            DoubleBuffered = true;

            Controls.Add(lblHint);
            Controls.Add(lstFunc);

            //MouseDown += new System.Windows.Forms.MouseEventHandler(this.pix_MouseDown);
            //MouseMove += new System.Windows.Forms.MouseEventHandler(this.pix_MouseMove);
        }

        protected override void OnResize(EventArgs e)
        {
            Refresh();
            base.OnResize(e);
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            Point pixMouse = PointToClient(Cursor.Position);

            Graphics g = e.Graphics;

            g.TranslateTransform(Width / 2, Height / 2);

            int half = 9 * Math.Min(Height, Width) / 20;

            g.DrawRectangle(Pens.Black, -half, -half, 2 * half, 2 * half);
            g.ResetTransform();
            pinout?.ForEach(pin => DrawPin(g, pin, pixMouse));

            base.OnPaint(e);
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            Pin p = pinout.Find(pin => GetPinRectangle(pin.pin).Contains(e.Location));
            if (p != null && !lstFunc.Visible)
            {
                Refresh();
                lblHint.Text =
                        $"Pin {p.pin}{Environment.NewLine}" +
                        string.Join(Environment.NewLine, p.func.Select(pf => $"{pf.periph} {pf.func}"));
                lblHint.Left = Math.Min(e.X + 16, Width - lblHint.Width);
                lblHint.Top = Math.Min(e.Y + 16, Height - lblHint.Height);
                lblHint.Show();
            }
            else
            {
                lblHint.Hide();
            }
            base.OnMouseMove(e);
        }

        protected override void OnMouseDown(MouseEventArgs e)
        {
            Pin p = pinout.Find(pin => GetPinRectangle(pin.pin).Contains(e.Location));
            if (p != null && p.func.Count > 1)
            {
                lstFunc.Items.Clear();
                lstFunc.Items.Add("undefined");
                lstFunc.Items.AddRange(
                    p.func.Select(pf => $"{pf.periph} {pf.func}").ToArray());
                lstFunc.Tag = p;
                lstFunc.Bounds = lblHint.Bounds;
                lstFunc.Show();
                lstFunc.Focus();
            }
            else
            {
                lstFunc.Hide();
                Refresh();
            }
            base.OnMouseDown(e);
        }
        private void lblHint_VisibleChanged(object sender, EventArgs e)
        {
            Refresh();
        }

        private void lstFunc_MouseMove(object sender, MouseEventArgs e)
        {
            lstFunc.SelectedIndex = lstFunc.IndexFromPoint(e.Location);
        }

        private void lstFunc_MouseDown(object sender, MouseEventArgs e)
        {
            string item = lstFunc.Items[lstFunc.IndexFromPoint(e.Location)].ToString();
            Pin pin = lstFunc.Tag as Pin;
            pin.sel = pin.func.FindIndex(pf => item.StartsWith(pf.periph) && item.EndsWith(pf.func));
            lstFunc.Hide();
            Refresh();
        }

        private void DrawPin(Graphics g, Pin pin, Point pixMouse)
        {
            int pinSide = (pin.pin - 1) / pinout.pinPerSide;
            int pinPos = (pin.pin - 1) % pinout.pinPerSide;

            int sz = 18 * Math.Min(Height, Width) / 20;

            int pinL = sz / 6 / 4;
            int pinW = sz / 6 / 8;
            int pinShift = sz * (pinPos + 2) / 40;

            g.ResetTransform();
            g.TranslateTransform(Width / 2, Height / 2);

            GraphicsState gState;
            SizeF fsz = g.MeasureString($"{pin.pin}", Font);
            float sc = Math.Min(1, Math.Min(pinL / fsz.Width, pinW / fsz.Height));

            float sideRot, textRot;
            PointF textOrigin;
            switch (pinSide)
            {
                case 0:
                    sideRot = 0;
                    textRot = 0;
                    textOrigin = new PointF(-fsz.Width, 0);
                    break;
                case 1:
                    sideRot = -90;
                    textRot = 0;
                    textOrigin = new PointF(-fsz.Width, 0);
                    break;
                case 2:
                    sideRot = -180;
                    textRot = 180;
                    textOrigin = new PointF(0, -fsz.Height);
                    break;
                case 3:
                    sideRot = -270;
                    textRot = 180;
                    textOrigin = new PointF(0, -fsz.Height);
                    break;
                default:
                    sideRot = textRot = 0;
                    textOrigin = PointF.Empty;
                    break;
            }

            g.RotateTransform(sideRot);
            g.TranslateTransform(-sz / 2, -sz / 2);
            g.TranslateTransform(0, pinShift);

            Rectangle rect = new Rectangle(-pinL, 0, pinL, pinW);

            Point[] pts = new Point[] { pixMouse, rect.Location };
            using (Matrix matrix = g.Transform.Clone())
            {
                matrix.Invert();
                matrix.TransformPoints(pts);
            }
            bool mouseHover = rect.Contains(pts[0]);
            bool periphSelected = pin.func.Exists(pf => pf.periph.Equals(SelectedPeriph));

            if (mouseHover)
                g.FillRectangle(Brushes.Gray, rect);
            else if (periphSelected)
            {
                g.FillRectangle(GetBrush(SelectedPeriph), rect);
                g.DrawRectangle(new Pen(Color.Black, 2), rect);
            }
            else if (pin.sel > -1)
            {
                g.FillRectangle(GetBrush(pin.func[pin.sel].periph), rect);
            }

            g.DrawRectangle(Pens.Black, rect);

            gState = g.Save();
            g.ScaleTransform(sc, sc);
            g.RotateTransform(textRot);
            g.DrawString($"{pin.pin}", base.Font, Brushes.Black, textOrigin);
            if (pin.sel > -1)
            {
                SizeF funcSz = g.MeasureString($"{pin.func[pin.sel].func}", base.Font);

                g.DrawString($"{pin.func[pin.sel].func}", base.Font, Brushes.Black,
                    new PointF(pinSide < 2 ? rect.Right + 5 : -funcSz.Width - 5, textOrigin.Y));
            }
            g.Restore(gState);
        }

        private static SolidBrush GetBrush(string periph)
        {
            return new SolidBrush(Color.FromArgb((int)($"{periph.GetHashCode()}".GetHashCode() | 0xFF808080)));
        }

        private Rectangle GetPinRectangle(int pin)
        {
            int pinSide = (pin - 1) / pinout.pinPerSide;
            int pinPos = (pin - 1) % pinout.pinPerSide;

            Rectangle rect = new Rectangle(Width / 2, Height / 2, 0, 0);
            int sz = 18 * Math.Min(Height, Width) / 20;

            int pinL = sz / 6 / 4;
            int pinW = sz / 6 / 8;

            int pinShift = sz * (pinPos + 2) / 40;
            switch (pinSide)
            {
                case 0:
                    rect.Offset(-sz / 2, -sz / 2);
                    rect.Offset(0, pinShift);
                    rect.Offset(-pinL, 0);
                    rect.Width = pinL;
                    rect.Height = pinW;
                    break;
                case 1:
                    rect.Offset(-sz / 2, sz / 2);
                    rect.Offset(pinShift, 0);
                    rect.Offset(0, 0);
                    rect.Width = pinW;
                    rect.Height = pinL;
                    break;
                case 2:
                    rect.Offset(sz / 2, sz / 2);
                    rect.Offset(0, -pinShift);
                    rect.Offset(0, -pinW);
                    rect.Width = pinL;
                    rect.Height = pinW;
                    break;
                case 3:
                    rect.Offset(sz / 2, -sz / 2);
                    rect.Offset(-pinShift, 0);
                    rect.Offset(-pinW, -pinL);
                    rect.Width = pinW;
                    rect.Height = pinL;
                    break;
                default:
                    rect = Rectangle.Empty;
                    break;
            }
            return rect;
        }
    }
}