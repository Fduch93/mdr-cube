﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;

namespace mdr32cube
{
    public partial class MdrCubeForm : Form
    {
        public MdrCubeForm()
        {
            InitializeComponent();

            /*Is setting protected properties via reflection ugly? 
             * Yes. 
             * Is sub-classing a control you want to use in the GUI designer even uglier? 
             * Also yes.
             * (c) Lily Finley @ https://stackoverflow.com/questions/818415/how-do-i-double-buffer-a-panel */

            typeof(Panel).InvokeMember("DoubleBuffered", BindingFlags.SetProperty
            | BindingFlags.Instance | BindingFlags.NonPublic, null,
            pix, new object[] { true });
        }

        private int pinPerSide = 36;
        List<Pin> pins = new List<Pin>();

        private void pix_Paint(object sender, PaintEventArgs e)
        {
            Point pixMouse = pix.PointToClient(Cursor.Position);

            Graphics g = e.Graphics;

            g.TranslateTransform(pix.Width / 2, pix.Height / 2);

            int half = 9 * Math.Min(pix.Height, pix.Width) / 20;

            g.DrawRectangle(Pens.Black, -half, -half, 2 * half, 2 * half);
            g.ResetTransform();
            pins.ForEach(pin => DrawPin(g, pin, pixMouse));
        }

        private void DrawPin(Graphics g, Pin pin, Point pixMouse)
        {
            int pinSide = (pin.pin - 1) / pinPerSide;
            int pinPos = (pin.pin - 1) % pinPerSide;

            int sz = 18 * Math.Min(pix.Height, pix.Width) / 20;

            int pinL = sz / 6 / 4;
            int pinW = sz / 6 / 8;
            int pinShift = sz * (pinPos + 2) / 40;

            g.ResetTransform();
            g.TranslateTransform(pix.Width / 2, pix.Height / 2);

            GraphicsState gState;
            SizeF fsz = g.MeasureString($"{pin.pin}", Font);
            float sc = Math.Min(1, Math.Min(pinL / fsz.Width, pinW / fsz.Height));

            float sideRot, textRot;
            PointF textOrigin;
            switch (pinSide)
            {
                case 0:
                    sideRot = 0;
                    textRot = 0;
                    textOrigin = new PointF(-fsz.Width, 0);
                    break;
                case 1:
                    sideRot = -90;
                    textRot = 0;
                    textOrigin = new PointF(-fsz.Width, 0);
                    break;
                case 2:
                    sideRot = -180;
                    textRot = 180;
                    textOrigin = new PointF(0, -fsz.Height);
                    break;
                case 3:
                    sideRot = -270;
                    textRot = 180;
                    textOrigin = new PointF(0, -fsz.Height);
                    break;
                default:
                    sideRot = textRot = 0;
                    textOrigin = PointF.Empty;
                    break;
            }

            g.RotateTransform(sideRot);
            g.TranslateTransform(-sz / 2, -sz / 2);
            g.TranslateTransform(0, pinShift);

            Rectangle rect = new Rectangle(-pinL, 0, pinL, pinW);

            Point[] pts = new Point[] { pixMouse, rect.Location };
            using (Matrix matrix = g.Transform.Clone())
            {
                matrix.Invert();
                matrix.TransformPoints(pts);
            }
            bool mouseHover = rect.Contains(pts[0]);
            bool periphSelected = pin.func.Exists(pf => pf.periph.Equals(lstPeriph.SelectedItem?.ToString()));

            if (mouseHover)
                g.FillRectangle(Brushes.Gray, rect);
            else if (periphSelected)
            {
                g.FillRectangle(GetBrush(lstPeriph.SelectedItem.ToString()), rect);
                g.DrawRectangle(new Pen(Color.Black, 2), rect);
            }
            else if (pin.sel > -1)
            {
                g.FillRectangle(GetBrush(pin.func[pin.sel].periph), rect);
            }

            g.DrawRectangle(Pens.Black, rect);

            gState = g.Save();
            g.ScaleTransform(sc, sc);
            g.RotateTransform(textRot);
            g.DrawString($"{pin.pin}", base.Font, Brushes.Black, textOrigin);
            if (pin.sel > -1)
            {
                SizeF funcSz = g.MeasureString($"{pin.func[pin.sel].func}", base.Font);

                g.DrawString($"{pin.func[pin.sel].func}", base.Font, Brushes.Black,
                    new PointF(pinSide < 2 ? rect.Right + 5 : -funcSz.Width - 5, textOrigin.Y));
            }
            g.Restore(gState);
        }

        private static SolidBrush GetBrush(string periph)
        {
            return new SolidBrush(Color.FromArgb((int)($"{periph.GetHashCode()}".GetHashCode() | 0xFF808080)));
        }

        private Rectangle GetPinRectangle(int pin)
        {
            int pinSide = (pin - 1) / pinPerSide;
            int pinPos = (pin - 1) % pinPerSide;

            Rectangle rect = new Rectangle(pix.Width / 2, pix.Height / 2, 0, 0);
            int sz = 18 * Math.Min(pix.Height, pix.Width) / 20;

            int pinL = sz / 6 / 4;
            int pinW = sz / 6 / 8;

            int pinShift = sz * (pinPos + 2) / 40;
            switch (pinSide)
            {
                case 0:
                    rect.Offset(-sz / 2, -sz / 2);
                    rect.Offset(0, pinShift);
                    rect.Offset(-pinL, 0);
                    rect.Width = pinL;
                    rect.Height = pinW;
                    break;
                case 1:
                    rect.Offset(-sz / 2, sz / 2);
                    rect.Offset(pinShift, 0);
                    rect.Offset(0, 0);
                    rect.Width = pinW;
                    rect.Height = pinL;
                    break;
                case 2:
                    rect.Offset(sz / 2, sz / 2);
                    rect.Offset(0, -pinShift);
                    rect.Offset(0, -pinW);
                    rect.Width = pinL;
                    rect.Height = pinW;
                    break;
                case 3:
                    rect.Offset(sz / 2, -sz / 2);
                    rect.Offset(-pinShift, 0);
                    rect.Offset(-pinW, -pinL);
                    rect.Width = pinW;
                    rect.Height = pinL;
                    break;
                default:
                    rect = Rectangle.Empty;
                    break;
            }
            return rect;
        }

        private void pix_Resize(object sender, EventArgs e) => pix.Refresh();

        private void pix_MouseMove(object sender, MouseEventArgs e)
        {
            Pin p = pins.Find(pin => GetPinRectangle(pin.pin).Contains(e.Location));
            if (p != null && !lstFunc.Visible)
            {
                pix.Refresh();
                lblHint.Text =
                        $"Pin {p.pin}{Environment.NewLine}" +
                        string.Join(Environment.NewLine, p.func.Select(pf => $"{pf.periph} {pf.func}"));
                lblHint.Left = Math.Min(e.X + 16, pix.Width - lblHint.Width);
                lblHint.Top = Math.Min(e.Y + 16, pix.Height - lblHint.Height);
                lblHint.Show();
            }
            else
            {
                lblHint.Hide();
            }
        }

        private void lstPeriph_SelectedIndexChanged(object sender, EventArgs e)
        {
            pix.Refresh();
            lstPeriph.Refresh();
        }

        private void lstPeriph_DrawItem(object sender, DrawItemEventArgs e)
        {
            if (e.Index < 0)
                return;

            string periph = lstPeriph.Items[e.Index].ToString();
            SolidBrush periphBrush = GetBrush(periph);
            e.Graphics.FillRectangle(periphBrush, e.Bounds);
            e.Graphics.DrawString(
                periph,
                lstPeriph.SelectedIndex != e.Index ? e.Font : new Font(e.Font, FontStyle.Bold),
                Brushes.Black,
                e.Bounds,
                StringFormat.GenericDefault);
        }

        private void lblHint_VisibleChanged(object sender, EventArgs e)
        {
            pix.Refresh();
        }

        private void pix_MouseDown(object sender, MouseEventArgs e)
        {
            Pin p = pins.Find(pin => GetPinRectangle(pin.pin).Contains(e.Location));
            if (p != null && p.func.Count > 1)
            {
                lstFunc.Items.Clear();
                lstFunc.Items.Add("undefined");
                lstFunc.Items.AddRange(
                    p.func.Select(pf => $"{pf.periph} {pf.func}").ToArray());
                lstFunc.Tag = p;
                lstFunc.Bounds = lblHint.Bounds;
                lstFunc.Show();
                lstFunc.Focus();
            }
            else
            {
                lstFunc.Hide();
                pix.Refresh();
            }
        }

        private void lstFunc_MouseMove(object sender, MouseEventArgs e)
        {
            lstFunc.SelectedIndex = lstFunc.IndexFromPoint(e.Location);
        }

        private void lstFunc_MouseDown(object sender, MouseEventArgs e)
        {
            string item = lstFunc.Items[lstFunc.IndexFromPoint(e.Location)].ToString();
            Pin pin = lstFunc.Tag as Pin;
            pin.sel = pin.func.FindIndex(pf => item.StartsWith(pf.periph) && item.EndsWith(pf.func));
            lstFunc.Hide();
            pix.Refresh();
        }

        private void btnOpen_Click(object sender, EventArgs e)
        {
            if (dlgOpen.ShowDialog() == DialogResult.OK)
            {
                pinout.FromString(File.ReadAllText(dlgOpen.FileName));
                pins = File.ReadAllLines(dlgOpen.FileName)
                    .Select(line => line.Split(';'))
                    .Select(parts => parts
                        .Select(part => part.Trim())
                        .Reverse()
                        .SkipWhile(string.IsNullOrWhiteSpace)
                        .Reverse()
                        .ToArray())
                    .Where(parts => parts.Length > 1 && ushort.TryParse(parts[0], out ushort _))
                    .Select(parts =>
                    {
                        if (parts.Length % 2 == 1 ||
                            !ushort.TryParse(parts.Last(), out ushort selectedFunc) ||
                            selectedFunc > parts.Length / 2 - 1)
                            return new Pin(ushort.Parse(parts[0]), parts.Skip(1).ToArray());
                        else
                            return new Pin(
                                ushort.Parse(parts[0]), parts.Skip(1).Take(parts.Length - 2).ToArray())
                            { sel = selectedFunc };
                    })
                    .ToList();

                pinPerSide = pins.Select(pin => pin.pin).Max() / 4;

                lstPeriph.Items.Clear();
                pins
                    .SelectMany(pin => pin.func.Select(pf => pf.periph))
                    .Distinct()
                    .ToList()
                    .ForEach(periph => lstPeriph.Items.Add(periph));

                pins
                    .Where(pin => pin.func.Count == 1)
                    .ToList()
                    .ForEach(pin => pin.sel = 0);

                dlgSave.FileName = dlgOpen.FileName;
                lblCurrentFile.Text = dlgOpen.FileName;

                pix.Refresh();
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (dlgSave.ShowDialog() == DialogResult.OK)
            {
                File.WriteAllLines(dlgSave.FileName,
                    pins.Select(pin =>
                        $"{pin.pin};" +
                        string.Join(";", pin.func.SelectMany(pf => new string[] { pf.periph, pf.func })) +
                        $"{((pin.sel > -1) ? $";{pin.sel}" : string.Empty)}"));

                lblCurrentFile.Text = dlgSave.FileName;
                dlgOpen.FileName = dlgSave.FileName;
            }
        }
    }
}
