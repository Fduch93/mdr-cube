﻿using System.Collections.Generic;

namespace mdr32cube
{
    static class VE1QI
    {
        private const string ETHERNET = "ETHERNET";
        private const string PWR = "PWR";
        private const string DEBUG = "DEBUG";
        private const string GPIO = "GPIO";
        private const string EXTBUS = "EXTBUS";
        private const string EXTINT = "EXTINT";
        private const string TIMER1 = "TIMER1";
        private const string TIMER2 = "TIMER2";
        private const string TIMER3 = "TIMER3";
        private const string TIMER4 = "TIMER3";
        private const string ARINC = "ARINC";
        private const string MIL = "MIL";
        private const string UART1 = "UART1";
        private const string UART2 = "UART2";
        private const string SPI1 = "SPI1";
        private const string SPI2 = "SPI2";
        private const string SPI3 = "SPI3";
        private const string CAN1 = "CAN1";
        private const string CAN2 = "CAN2";
        private const string ADC = "ADC";
        private const string DAC = "DAC";

        public static List<Pin> pins = new List<Pin>()
        {
            new Pin(  1, ETHERNET, "VSS1A"),
            new Pin(  2, ETHERNET, "TXN"),
            new Pin(  3, ETHERNET, "TXP"),
            new Pin(  4, ETHERNET, "VSS1A"),
            new Pin(  5, ETHERNET, "VDD1A"),
            new Pin(  6, ETHERNET, "CM"),
            new Pin(  7, ETHERNET, "RXN"),
            new Pin(  8, ETHERNET, "RXP"),
            new Pin(  9, ETHERNET, "TSTBUSA"),
            new Pin( 10, ETHERNET, "VDD2A"),
            new Pin( 11, ETHERNET, "VSS2A"),
            new Pin( 12, ETHERNET, "EXTRES1"),
            new Pin( 13, ETHERNET, "VSS3A"),
            new Pin( 14, ETHERNET, "VDD3A"),
            new Pin( 15, ETHERNET, "VSS4A"),
            new Pin( 16, ETHERNET, "VDD4A"),

            new Pin( 17, string.Empty, "ITCMLAEN"),

            new Pin( 18, GPIO, "PE3", TIMER1, "CH1", EXTBUS, "A17" ),
            new Pin( 19, GPIO, "PE4", TIMER1, "CH2", EXTBUS, "A18" ),
            new Pin( 20, GPIO, "PE5", TIMER1, "CH3", EXTBUS, "A19" ),
            new Pin( 21, GPIO, "PE8", TIMER2, "CH2", EXTBUS, "A22" ),
            new Pin( 22, GPIO, "PE9", TIMER2, "CH3", EXTBUS, "A23" ),
            new Pin( 23, GPIO, "PE10", TIMER2, "CH4", EXTBUS, "A24" ),
            new Pin( 24, GPIO, "PE11", CAN1, "RX",    EXTBUS, "A25" ),
            new Pin( 25, GPIO, "PE12", CAN1, "TX",    EXTBUS, "A26" ),
            new Pin( 26, GPIO, "PE13", CAN2, "RX",    EXTBUS, "A27" ),
            new Pin( 27, GPIO, "PE14", CAN2, "TX",    EXTBUS, "A28" ),
            new Pin( 28, GPIO, "PE15", MIL, "INOUT_D",EXTBUS, "A29" ),

            new Pin( 29, GPIO, "PF0", ETHERNET, "OSC25IN" , MIL, "INOUT_A", EXTBUS, "RDY" ),
            new Pin( 30, GPIO, "PF1", ETHERNET, "OSC25OUT", MIL, "INOUT_B", EXTBUS, "A30"  ),
            new Pin( 31, GPIO, "PF2", EXTBUS, "RDY", MIL, "INOUT_C", EXTBUS, "A31" ),

            new Pin( 32, PWR, "GND"),
            new Pin( 33, PWR, "Ucc"),
            new Pin( 34, PWR, "BUcc"),

            new Pin( 35, DEBUG, "JTAGEN"),

            new Pin( 36, PWR, "VDD"),

            new Pin( 37, GPIO, "PE6", string.Empty, "OSC32IN" , TIMER1, "CH4", EXTBUS, "A20"),
            new Pin( 38, GPIO, "PE7", string.Empty, "OSC32OUT", TIMER2, "CH1", EXTBUS, "A21"),

            new Pin( 39, string.Empty, "WAKE"),
            new Pin( 40, string.Empty, "SHDN"),
            new Pin( 41, string.Empty, "EXT_POR"),

            new Pin( 42, "USB", "DP"),
            new Pin( 43, "USB", "DN"),

            new Pin( 44, PWR, "AGND"),
            new Pin( 45, PWR, "AUcc"),

            new Pin( 46, GPIO, "PD7",  ADC, "AIN0", SPI2, "TX",   UART2, "nDCD", EXTBUS, "A5"),
            new Pin( 47, GPIO, "PD8",  ADC, "AIN1", SPI2, "RX",   UART2, "nDTR", EXTBUS, "A4"),
            new Pin( 48, GPIO, "PD9",  ADC, "AIN2", SPI2, "SCK",  UART2, "nDSR", EXTBUS, "A3"),
            new Pin( 49, GPIO, "PD10", ADC, "AIN3", SPI2, "FSS",  UART2, "nRTS", EXTBUS, "A2"),
            new Pin( 50, GPIO, "PD11", ADC, "AIN4", EXTBUS, "A0", UART2, "nCTS" ),
            new Pin( 51, GPIO, "PD12", ADC, "AIN5", SPI3, "TX",   TIMER3, "ETR3", SPI3, "RX"),
            new Pin( 52, GPIO, "PD13", ADC, "AIN6", UART2, "Tx", ARINC, "OUT1A", UART1, "SIROUT" ),
            new Pin( 53, GPIO, "PD14", ADC, "AIN7", UART2, "Rx", ARINC, "OUT1B", UART1, "SIRIN"  ),
            new Pin( 54, GPIO, "PD15", DAC, "REF0", ARINC, "OUT3A", EXTBUS, "A13" ),

            new Pin( 55, GPIO, "PE0", DAC, "REF1",  ARINC, "OUT4A", EXTBUS, "A14" ),
            new Pin( 56, GPIO, "PE1", DAC, "AOUT0", ARINC, "OUT3B", EXTBUS, "A15", UART2, "nRI" ),
            new Pin( 57, GPIO, "PE2", DAC, "AOUT1", ARINC, "OUT4B", EXTBUS, "A16" ),

            new Pin( 58, string.Empty, "OSC_IN"),
            new Pin( 59, string.Empty, "OSC_OUT"),

            new Pin( 60, string.Empty, "nRESET"),

            new Pin( 61, GPIO, "PC5",  EXTINT, "EXT1", SPI1, "TX" , SPI1, "RX"),
            new Pin( 62, GPIO, "PC6",  EXTINT, "EXT2", SPI1, "RX" , SPI1, "TX"),
            new Pin( 63, GPIO, "PC7",  EXTINT, "EXT3", SPI1, "SCK"),
            new Pin( 64, GPIO, "PC8",  EXTINT, "EXT4", SPI1, "FSS"),
            new Pin( 65, GPIO, "PC9",  SPI2, "TX" , EXTBUS, "BE0", CAN1, "RX" ),
            new Pin( 66, GPIO, "PC10", SPI2, "RX" , EXTBUS, "BE1", CAN1, "TX" ),
            new Pin( 67, GPIO, "PC11", SPI2, "SCK", EXTBUS, "BE2", CAN2, "RX" ),
            new Pin( 68, GPIO, "PC12", SPI2, "FSS", EXTBUS, "BE3", CAN2, "TX" ),

            new Pin( 69, GPIO, "PF13", ARINC, "OUT2A", EXTBUS, "A10", SPI3, "FSS"),
            new Pin( 70, GPIO, "PF14", ARINC, "OUT2B", EXTBUS, "A11", SPI3, "SCK"),
            new Pin( 71, GPIO, "PF15", SPI3, "RX",     EXTBUS, "A12", SPI3, "TXD" ),

            new Pin( 72, GPIO, "PC13", MIL, "IN_A_P", EXTBUS, "A30", UART2, "Tx" ),
            new Pin( 73, GPIO, "PC14", MIL, "IN_A_N", EXTBUS, "A31", UART2, "Rx" ),
            new Pin( 74, GPIO, "PC15", MIL, "IN_B_P", EXTBUS, "BUSY", TIMER2, "CH1"),

            new Pin( 75, PWR, "GND"),
            new Pin( 76, PWR, "Ucc"),
            new Pin( 77, PWR, "VPP"),

            new Pin( 78, string.Empty, "TM1"),
            new Pin( 79, string.Empty, "TM0"),
            new Pin( 80, string.Empty, "TM2"),

            new Pin( 81, GPIO, "PD0", MIL, "IN_B_N" , EXTBUS, "ALE", EXTBUS, "A16"),
            new Pin( 82, GPIO, "PD1", MIL, "OUT_A_P", EXTBUS, "CLE", EXTBUS, "A15"),
            new Pin( 83, GPIO, "PD2", MIL, "OUT_A_N", SPI1, "TX",    EXTBUS, "A14"),
            new Pin( 84, GPIO, "PD3", MIL, "OUT_B_P", SPI1, "RX",    EXTBUS, "A13"),
            new Pin( 85, GPIO, "PD4", MIL, "OUT_B_N", SPI1, "SCK",   EXTBUS, "A7"),
            new Pin( 86, GPIO, "PD5", MIL, "INOUT_A", SPI1, "FSS",   EXTBUS, "A6"),
            new Pin( 87, GPIO, "PD6", MIL, "INOUT_B", UART2, "nRI",  EXTBUS, "A5"),

            new Pin( 88, GPIO, "PF3",  MIL, "IN_C_P",  EXTBUS, "A0", TIMER1, "CH1" ),
            new Pin( 89, GPIO, "PF4",  MIL, "IN_C_N",  EXTBUS, "A1", TIMER1, "CH2" ),
            new Pin( 90, GPIO, "PF5",  MIL, "IN_D_P",  EXTBUS, "A2", TIMER1, "CH3" ),
            new Pin( 91, GPIO, "PF6",  MIL, "IN_D_N",  EXTBUS, "A3", TIMER1, "CH4" ),
            new Pin( 92, GPIO, "PF7",  MIL, "OUT_C_P", EXTBUS, "A4", ARINC, "OUT4A" ),
            new Pin( 93, GPIO, "PF8",  MIL, "OUT_C_N", EXTBUS, "A5", ARINC, "OUT4B" ),
            new Pin( 94, GPIO, "PF9",  MIL, "OUT_D_P", EXTBUS, "A6", ARINC, "OUT3A" ),
            new Pin( 95, GPIO, "PF10", MIL, "OUT_D_N", EXTBUS, "A7", ARINC, "OUT3B" ),
            new Pin( 96, GPIO, "PF11", MIL, "INOUT_C", EXTBUS, "A8", ARINC, "OUT2A" ),
            new Pin( 97, GPIO, "PF12", MIL, "INOUT_D", EXTBUS, "A9", ARINC, "OUT2B" ),

            new Pin( 98, GPIO, "PC0", EXTBUS, "nWR", TIMER1, "ETR1", TIMER1, "BRK1" ),
            new Pin( 99, GPIO, "PC1", EXTBUS, "nRD", TIMER2, "ETR2", TIMER2, "BRK2" ),
            new Pin(100, GPIO, "PC2", EXTBUS, "ALE", EXTBUS, "CLKO", TIMER3, "BRK3" ),
            new Pin(101, GPIO, "PC3", UART1, "Tx",   EXTBUS, "CLE",  UART1, "SIROUT"),
            new Pin(102, GPIO, "PC4", UART1, "Rx",   EXTBUS, "BUSY", UART1, "SIRIN" ),

            new Pin(103, GPIO, "PB15", EXTBUS, "D31", ARINC, "IN8B", TIMER2, "CH4N" ),
            new Pin(104, GPIO, "PB14", EXTBUS, "D30", ARINC, "IN8A", TIMER1, "CH4N" ),
            new Pin(105, GPIO, "PB13", EXTBUS, "D29", ARINC, "IN7B", TIMER2, "CH3N" ),
            new Pin(106, GPIO, "PB12", EXTBUS, "D28", ARINC, "IN7A", TIMER1, "CH3N" ),
            new Pin(107, GPIO, "PB11", EXTBUS, "D27", ARINC, "IN6B", TIMER2, "CH2N" ),
            new Pin(108, GPIO, "PB10", EXTBUS, "D26", ARINC, "IN6A", TIMER1, "CH2N" ),
            new Pin(109, GPIO, "PB9",  EXTBUS, "D25", ARINC, "IN5B", TIMER2, "CH1N" ),
            new Pin(110, GPIO, "PB8",  EXTBUS, "D24", ARINC, "IN5A", TIMER1, "CH1N" ),
            new Pin(111, GPIO, "PB7",  EXTBUS, "D23", ARINC, "IN4B", TIMER3, "CH4N" ),
            new Pin(112, GPIO, "PB6",  EXTBUS, "D22", ARINC, "IN4A", TIMER3, "CH4"  ),
            new Pin(113, GPIO, "PB5",  EXTBUS, "D21", ARINC, "IN3B", TIMER3, "CH3N" ),
            new Pin(114, GPIO, "PB4",  EXTBUS, "D20", ARINC, "IN3A", TIMER3, "CH3"  ),
            new Pin(115, GPIO, "PB3",  EXTBUS, "D19", ARINC, "IN2B", TIMER3, "CH2N" ),
            new Pin(116, GPIO, "PB2",  EXTBUS, "D18", ARINC, "IN2A", TIMER3, "CH2"  ),
            new Pin(117, GPIO, "PB1",  EXTBUS, "D17", ARINC, "IN1B", TIMER3, "CH1N" ),
            new Pin(118, GPIO, "PB0",  EXTBUS, "D16", ARINC, "IN1A", TIMER3, "CH1"  ),

            new Pin(119, GPIO, "PA15", EXTBUS, "D15", TIMER4, "ETR4", MIL, "OUT_D_N" ),
            new Pin(120, GPIO, "PA14", EXTBUS, "D14", TIMER4, "BRK4", MIL, "OUT_D_P" ),
            new Pin(121, GPIO, "PA13", EXTBUS, "D13", TIMER4, "CH4N", MIL, "OUT_C_N" ),
            new Pin(122, GPIO, "PA12", EXTBUS, "D12", TIMER4, "CH4",  MIL, "OUT_C_P" ),
            new Pin(123, GPIO, "PA11", EXTBUS, "D11", TIMER4, "CH3N", MIL, "IN_D_N"  ),
            new Pin(124, GPIO, "PA10", EXTBUS, "D10", TIMER4, "CH3",  MIL, "IN_D_P"  ),

            new Pin(125, PWR, "GND"),
            new Pin(126, string.Empty, "PWRST_BYP"),
            new Pin(127, PWR, "Ucc"),

            new Pin(128, GPIO, "PA9", EXTBUS, "D9", TIMER4, "CH2N", MIL, "IN_C_N"  ),
            new Pin(129, GPIO, "PA8", EXTBUS, "D8", TIMER4, "CH2",  MIL, "IN_C_P"  ),
            new Pin(130, GPIO, "PA7", EXTBUS, "D7", TIMER4, "CH1N"  ),
            new Pin(131, GPIO, "PA6", EXTBUS, "D6", TIMER4, "CH1"  ),
            new Pin(132, GPIO, "PA5", EXTBUS, "D5", TIMER3, "BRK3"  ),
            new Pin(133, GPIO, "PA4", EXTBUS, "D4", TIMER2, "BRK2"  ),
            new Pin(134, GPIO, "PA3", EXTBUS, "D3", EXTINT, "EXT4", TIMER1, "BRK1" ),
            new Pin(135, GPIO, "PA2", EXTBUS, "D2", EXTINT, "EXT3", TIMER3, "ETR3" ),
            new Pin(136, GPIO, "PA1", EXTBUS, "D1", EXTINT, "EXT2", TIMER2, "ETR2" ),
            new Pin(137, GPIO, "PA0", EXTBUS, "D0", EXTINT, "EXT1", TIMER1, "ETR1" ),

            new Pin(138, DEBUG, "TMS"),
            new Pin(139, DEBUG, "TDI"),
            new Pin(140, DEBUG, "TDO"),
            new Pin(141, DEBUG, "TCK"),
            new Pin(142, DEBUG, "TRST"),

            new Pin(143, string.Empty, "NC"),
            new Pin(144, string.Empty, "NC"),
        };

    }
}
