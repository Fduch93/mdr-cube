﻿
namespace mdr32cube
{
    partial class PinoutPix
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.lstFunc = new System.Windows.Forms.ListBox();
            this.lblHint = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lstFunc
            // 
            this.lstFunc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lstFunc.FormattingEnabled = true;
            this.lstFunc.Location = new System.Drawing.Point(0, 0);
            this.lstFunc.Name = "lstFunc";
            this.lstFunc.Size = new System.Drawing.Size(120, 93);
            this.lstFunc.TabIndex = 4;
            this.lstFunc.Visible = false;
            this.lstFunc.MouseDown += new System.Windows.Forms.MouseEventHandler(this.lstFunc_MouseDown);
            this.lstFunc.MouseMove += new System.Windows.Forms.MouseEventHandler(this.lstFunc_MouseMove);
            // 
            // lblHint
            // 
            this.lblHint.AutoSize = true;
            this.lblHint.BackColor = System.Drawing.SystemColors.Control;
            this.lblHint.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblHint.Location = new System.Drawing.Point(0, 0);
            this.lblHint.Margin = new System.Windows.Forms.Padding(0);
            this.lblHint.Name = "lblHint";
            this.lblHint.Size = new System.Drawing.Size(100, 23);
            this.lblHint.TabIndex = 3;
            this.lblHint.VisibleChanged += new System.EventHandler(this.lblHint_VisibleChanged);
            // 
            // PinoutPix
            // 
            this.BackColor = System.Drawing.SystemColors.ControlLight;
            this.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox lstFunc;
        private System.Windows.Forms.Label lblHint;
    }
}
