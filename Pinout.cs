﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace mdr32cube
{
    public partial class Pinout : Component
    {
        public List<Pin> pins = new List<Pin>();
        public int pinPerSide => pins.Select(pin => pin.pin).Max() / 4;

        public Pinout()
        {
            InitializeComponent();
        }

        public Pinout(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        public Pin this[int index] => pins[index];
        public Pin this[string portPin] => pins.Find(
            pin => pin.func.Exists(
                pf => pf.func.Equals("GPIO") && pf.func.Equals(portPin, StringComparison.OrdinalIgnoreCase)));

        public void ForEach(Action<Pin> pinConsumer) => pins.ForEach(pinConsumer);

        public Pin Find(Predicate<Pin> pinPredicate) => pins.Find(pinPredicate);

        public void FromString(string src)
        {
            pins = Regex.Replace(src, @"\r\n|\n\r|\n|\r", Environment.NewLine)
                .Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries)
                .Select(line => line.Split(';')
                    .Select(linePart => linePart.Trim())
                    .Reverse()
                    .SkipWhile(string.IsNullOrWhiteSpace)
                    .Reverse()
                    .ToArray())
                .Where(lineParts => lineParts.Length > 1 && ushort.TryParse(lineParts[0], out ushort _))
                .Select(parts => new Pin(
                    ushort.Parse(parts[0]),
                    parts.Skip(1).ToArray(),
                    (parts.Length % 2 == 0 &&
                        ushort.TryParse(parts.Last(), out ushort selectedFunc) &&
                        selectedFunc <= parts.Length / 2 - 1)
                        ? selectedFunc
                        : parts.Length == 3
                            ? 0
                            : Pin.SEL_NONE))
                .ToList();
        }
    }
}
