﻿
namespace mdr32cube
{
    partial class MdrCubeForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.ToolStrip tlstrip;
            this.btnOpen = new System.Windows.Forms.ToolStripButton();
            this.btnSave = new System.Windows.Forms.ToolStripButton();
            this.lblCurrentFile = new System.Windows.Forms.ToolStripLabel();
            this.lstPeriph = new System.Windows.Forms.ListBox();
            this.pix = new System.Windows.Forms.Panel();
            this.lstFunc = new System.Windows.Forms.ListBox();
            this.lblHint = new System.Windows.Forms.Label();
            this.dlgOpen = new System.Windows.Forms.OpenFileDialog();
            this.dlgSave = new System.Windows.Forms.SaveFileDialog();
            this.pinoutPix1 = new mdr32cube.PinoutPix();
            this.pinout = new mdr32cube.Pinout(this.components);
            tlstrip = new System.Windows.Forms.ToolStrip();
            tlstrip.SuspendLayout();
            this.pix.SuspendLayout();
            this.SuspendLayout();
            // 
            // tlstrip
            // 
            tlstrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            tlstrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnOpen,
            this.btnSave,
            this.lblCurrentFile});
            tlstrip.Location = new System.Drawing.Point(154, 0);
            tlstrip.Name = "tlstrip";
            tlstrip.Size = new System.Drawing.Size(761, 25);
            tlstrip.TabIndex = 5;
            // 
            // btnOpen
            // 
            this.btnOpen.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnOpen.Image = global::mdr32cube.Properties.Resources.FileOpen;
            this.btnOpen.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnOpen.Name = "btnOpen";
            this.btnOpen.Size = new System.Drawing.Size(23, 22);
            this.btnOpen.Text = "Open";
            this.btnOpen.Click += new System.EventHandler(this.btnOpen_Click);
            // 
            // btnSave
            // 
            this.btnSave.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnSave.Image = global::mdr32cube.Properties.Resources.FileSave;
            this.btnSave.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(23, 22);
            this.btnSave.Text = "toolStripButton2";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // lblCurrentFile
            // 
            this.lblCurrentFile.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.lblCurrentFile.Name = "lblCurrentFile";
            this.lblCurrentFile.Size = new System.Drawing.Size(42, 22);
            this.lblCurrentFile.Text = "No file";
            this.lblCurrentFile.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lstPeriph
            // 
            this.lstPeriph.Dock = System.Windows.Forms.DockStyle.Left;
            this.lstPeriph.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
            this.lstPeriph.FormattingEnabled = true;
            this.lstPeriph.Location = new System.Drawing.Point(0, 0);
            this.lstPeriph.Name = "lstPeriph";
            this.lstPeriph.Size = new System.Drawing.Size(154, 542);
            this.lstPeriph.Sorted = true;
            this.lstPeriph.TabIndex = 2;
            this.lstPeriph.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.lstPeriph_DrawItem);
            this.lstPeriph.SelectedIndexChanged += new System.EventHandler(this.lstPeriph_SelectedIndexChanged);
            // 
            // pix
            // 
            this.pix.BackColor = System.Drawing.SystemColors.ControlLight;
            this.pix.Controls.Add(this.pinoutPix1);
            this.pix.Controls.Add(this.lstFunc);
            this.pix.Controls.Add(this.lblHint);
            this.pix.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pix.Location = new System.Drawing.Point(154, 25);
            this.pix.Name = "pix";
            this.pix.Size = new System.Drawing.Size(761, 517);
            this.pix.TabIndex = 4;
            this.pix.Paint += new System.Windows.Forms.PaintEventHandler(this.pix_Paint);
            this.pix.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pix_MouseDown);
            this.pix.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pix_MouseMove);
            this.pix.Resize += new System.EventHandler(this.pix_Resize);
            // 
            // lstFunc
            // 
            this.lstFunc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lstFunc.FormattingEnabled = true;
            this.lstFunc.Location = new System.Drawing.Point(0, 18);
            this.lstFunc.Name = "lstFunc";
            this.lstFunc.Size = new System.Drawing.Size(51, 41);
            this.lstFunc.TabIndex = 4;
            this.lstFunc.Visible = false;
            this.lstFunc.MouseDown += new System.Windows.Forms.MouseEventHandler(this.lstFunc_MouseDown);
            this.lstFunc.MouseMove += new System.Windows.Forms.MouseEventHandler(this.lstFunc_MouseMove);
            // 
            // lblHint
            // 
            this.lblHint.AutoSize = true;
            this.lblHint.BackColor = System.Drawing.SystemColors.Control;
            this.lblHint.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblHint.Location = new System.Drawing.Point(0, 0);
            this.lblHint.Margin = new System.Windows.Forms.Padding(0);
            this.lblHint.Name = "lblHint";
            this.lblHint.Size = new System.Drawing.Size(26, 15);
            this.lblHint.TabIndex = 3;
            this.lblHint.Text = "hint";
            this.lblHint.VisibleChanged += new System.EventHandler(this.lblHint_VisibleChanged);
            // 
            // dlgOpen
            // 
            this.dlgOpen.DefaultExt = "csv";
            this.dlgOpen.Filter = "CSV (*.csv)|*.csv|All files (*.*)|*.*";
            this.dlgOpen.RestoreDirectory = true;
            // 
            // dlgSave
            // 
            this.dlgSave.DefaultExt = "csv";
            this.dlgSave.FileName = "MDR";
            this.dlgSave.Filter = "CSV (*.csv)|*.csv|All files (*.*)|*.*";
            // 
            // pinoutPix1
            // 
            this.pinoutPix1.BackColor = System.Drawing.SystemColors.ControlLight;
            this.pinoutPix1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pinoutPix1.Location = new System.Drawing.Point(0, 0);
            this.pinoutPix1.Name = "pinoutPix1";
            this.pinoutPix1.pinout = this.pinout;
            this.pinoutPix1.Size = new System.Drawing.Size(761, 517);
            this.pinoutPix1.TabIndex = 5;
            // 
            // MdrCubeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(915, 542);
            this.Controls.Add(this.pix);
            this.Controls.Add(tlstrip);
            this.Controls.Add(this.lstPeriph);
            this.DoubleBuffered = true;
            this.Name = "MdrCubeForm";
            this.Text = "MdrCube";
            tlstrip.ResumeLayout(false);
            tlstrip.PerformLayout();
            this.pix.ResumeLayout(false);
            this.pix.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ListBox lstPeriph;
        private System.Windows.Forms.Panel pix;
        private System.Windows.Forms.Label lblHint;
        private System.Windows.Forms.ListBox lstFunc;
        private System.Windows.Forms.ToolStripButton btnOpen;
        private System.Windows.Forms.ToolStripButton btnSave;
        private System.Windows.Forms.OpenFileDialog dlgOpen;
        private System.Windows.Forms.SaveFileDialog dlgSave;
        private System.Windows.Forms.ToolStripLabel lblCurrentFile;
        private PinoutPix pinoutPix1;
        private Pinout pinout;
    }
}

