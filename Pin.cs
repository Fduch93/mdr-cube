﻿using System.Collections.Generic;
using System.Drawing;

namespace mdr32cube
{
    public class Pin
    {
        public struct PeriphFunc
        {
            public string periph;
            public string func;
        }

        public ushort pin;
        public int sel;
        public List<PeriphFunc> func;

        public const int SEL_NONE = -1;

        public Pin(ushort pin, string[] periph_func, int sel = -1)
        {
            this.pin = pin;
            this.sel = sel;

            func = new List<PeriphFunc>();

            for (int i = 0; i < periph_func.Length / 2; i++)
                func.Add(new PeriphFunc() { periph = periph_func[i * 2], func = periph_func[i * 2 + 1] });
        }
    }
}
